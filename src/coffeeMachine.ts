import { ParameterError, ExceededError, CheckError, PowerError } from './machineError'

interface CoffeeMachine {
	switchPower(): void
	make(): void
}

export default class CoffeeMachineImpl implements CoffeeMachine {
	water = 0 // 물
	beans = 0 // 원두
	private power = false // 전원

	// 주문 (메뉴이름, 샷, 잔 수, 얼음 유무)
	protected order = {
		menuName: '',
		shot: 0,
		cups: 0,
		ice: false,
	}

	// 커피 추출 시 필요한 물과 원두의 양
	protected coffeeIngredient = {
		reqWater: 0,
		reqBean: 0,
	}

	private waterLimit = 1000 // 물의 양 한도
	private beansLimit = 1000 // 원두의 양 한도

	// 완성한 주문 출력 log
	private logOrder(): void {
		console.log(`${this.order.shot}샷의 ${this.order.menuName} ${this.order.cups}잔 나왔습니다.`)
	}

	// 전원의 유효성 체크, 전원을 키면 얼음 추가, 샷 추가, 주문할 잔 수 설정 가능
	private powerCheck(): void {
		if (!this.power) throw new PowerError()
	}

	// 물의 양 유효성 체크
	private waterCheck(): void {
		if (this.water < this.requireWater) throw new CheckError('water')
	}

	// 원두 양 유효성 체크
	private beansCheck(): void {
		if (this.beans < this.requireBean) throw new CheckError('beans')
	}

	// 주무한 메뉴에 따라 필요한 물과 원두를 사용 후 현재 물과 원두를 저장
	private drop(): void {
		this.water -= this.requireWater
		this.beans -= this.requireBean
	}

	// 추출의 필요한 총 물의 양
	get requireWater() {
		return this.coffeeIngredient.reqWater * this.order.cups
	}

	//추출의 필요한 총 원두의 양
	get requireBean() {
		return this.coffeeIngredient.reqBean * this.order.shot * this.order.cups
	}

	// power 설정
	switchPower(): void {
		this.power = !this.power
	}

	// 주입하는 물의 유효성을 체크 후 물 충전
	setWater(waterInput: number): void {
		if (waterInput < 0 || typeof waterInput !== 'number') throw new ParameterError('Wrong waterInput')
		else if (this.water + waterInput > this.waterLimit) throw new ExceededError(waterInput)

		this.water += waterInput
	}

	// 주입하는 원두의 유효성을 체크 후 원두 충전
	setBean(beanInput: number): void {
		if (beanInput < 0 || typeof beanInput !== 'number') throw new ParameterError('Wrong beanInput')
		else if (this.beans + beanInput > this.beansLimit) throw new ExceededError(beanInput)

		this.beans += beanInput
	}

	// 입력한 샷을 주문의 추가
	addShot(shotInput: number): void {
		this.powerCheck()

		if (shotInput < 0 || typeof shotInput !== 'number') throw new ParameterError('Wrong shotInput')
		this.order.shot = shotInput
	}

	// 얼음을 true 로 설정하면 메뉴이름에 '아이스' 추가
	switchIce(): void {
		this.powerCheck()

		this.order.ice = !this.order.ice

		if (this.order.ice) this.order.menuName = '아이스'.concat(this.order.menuName)
		else this.order.menuName = this.order.menuName.replace('아이스', '')
	}

	// 원하는 커피 잔 수를 설정
	setCups(cupInput: number): void {
		this.powerCheck()

		if (cupInput < 0 || typeof cupInput !== 'number') throw new ParameterError('wrong cupInput')
		this.order.cups = cupInput
	}

	// 전원, 물, 원두의 상태를 보고 추출
	make(): void {
		this.powerCheck()
		this.waterCheck()
		this.beansCheck()
		this.drop()
		this.logOrder()
	}
}
