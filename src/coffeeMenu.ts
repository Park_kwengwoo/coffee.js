import CoffeeMachineImpl from './coffeeMachine'
import { ExceededError, ParameterError } from './machineError'
export class Americano extends CoffeeMachineImpl {
	menuName = '아메리카노'

	constructor() {
		super()
		this.order.menuName = this.menuName
		this.coffeeIngredient.reqWater = 200 // 아메리카노 제조시 필요한 물의 양
		this.coffeeIngredient.reqBean = 50 // 아메리카노 제조시 필요한 원두의 양
	}
}

export class Latte extends CoffeeMachineImpl {
	menuName = '카페 라떼'
	milk = 0

	latteIngredient = {
		// 라때 제조시 필요한 우유의 양
		reqMilk: 0,
	}

	private milkLimit = 500 // 우유의 양 한도

	constructor() {
		super()
		this.order.menuName = this.menuName
		this.coffeeIngredient.reqWater = 200 // 라떼 제조시 필요한 물의양
		this.coffeeIngredient.reqBean = 50 // 라떼 제조시 필요한 원두의 양
		this.coffeeIngredient = Object.assign(this.coffeeIngredient, this.latteIngredient)
	}

	// 우유 추가
	addMilk(milkInput: number): void {
		if (milkInput < 0 || typeof milkInput !== 'number') throw new ParameterError('Wrong milkInput')
		else if (this.milk + milkInput > this.milkLimit) throw new ExceededError(milkInput)

		this.milk += milkInput
	}
}

export class Esspresso extends CoffeeMachineImpl {
	menuName = '에스프레소'

	constructor() {
		super()
		this.order.menuName = this.menuName
		this.coffeeIngredient.reqWater = 100 // 에스프레소 제조시 필요한 물의 양
		this.coffeeIngredient.reqBean = 100 // 에스프레소 제조시 필요한 원두의 양
	}
}
