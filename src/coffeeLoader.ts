import scanf from 'scanf'
import CoffeeMachineImpl from './coffeeMachine'
import { Americano, Latte, Esspresso } from './coffeeMenu'
export default function coffeeLoader() {
	let movv: CoffeeMachineImpl

	// 기계가 비동직적으로 돌아가기 위한 딜레이
	const delay = (): Promise<void> => {
		return new Promise(res => setTimeout(res => console.log('...가동중'), 2000))
	}

	async function loading(): Promise<void> {
		console.log('커피 머신을 작동합니다.')
		await delay()
	}

	async function selectMenu(): Promise<CoffeeMachineImpl> {
		console.log('원하시는 메뉴를 선택해주세요')
		console.log('1.아메리카노 2.카페라때 3.에스프레소')

		let menuInput = scanf('%d')

		if (menuInput)
			switch (menuInput) {
				case 1:
					return (movv = new Americano())
					break
				case 2:
					return (movv = new Latte())
					break
				case 3:
					return (movv = new Esspresso())
					break
				default:
			}
	}
}
