// 전원이 켜져있지 않을 때 발생하는 에러
export class PowerError extends Error {
	constructor() {
		super('Please Power ON')
		this.name = 'PowerError'
	}
}

// 잘못된 입력 값을 넣었을 때 발생하는 에러
export class ParameterError extends Error {
	constructor(message: string) {
		super(message)
		this.name = this.constructor.name
	}
}

//입력 값이 초과를 일으킬 시 발생하는 에러
export class ExceededError extends ParameterError {
	property: number
	constructor(proerty: number) {
		super('Exceeded ' + proerty)
		this.property = proerty
	}
}

// 유효성 검사 중 발생하는 에러
export class CheckError extends Error {
	property: string
	constructor(property: string) {
		super('CheckError ' + property)
		this.property = property
		this.name = this.constructor.name
	}
}
